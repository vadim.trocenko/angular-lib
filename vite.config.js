import path from 'path';
import { defineConfig } from 'vite';
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';

export default defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, 'lib/angular/index.js'),
      name: 'angular-lib',
      fileName: 'angular-lib',
      formats: ['es', 'cjs']
    },
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: true,
    emptyOutDir: true
  },
  plugins: [
    cssInjectedByJsPlugin()
  ]
});
