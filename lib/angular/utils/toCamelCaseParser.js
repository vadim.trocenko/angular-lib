export default function toCamelCaseParser(str) {
  const words = str.replace(/[-_]/gi, ' ').split(' ');

  return words.map((elem, index) => {
    if (index === 0) {
      return elem;
    } else {
      return elem[0].toUpperCase() + elem.slice(1);
    }
  }).join('');
}
