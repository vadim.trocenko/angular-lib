export default function componentParser(node) {
  let string = node.innerHTML;
  const matchArguments = string.match(/{{[a-zA-Z.]*}}/gi);

  if (!matchArguments) {
    return node;
  }

  matchArguments.forEach(element => {
    string = string.replace(new RegExp(element, 'g'), eval(String(element.match(/[a-zA-Z.]+/gi))));
  });

  node.innerHTML = string;
}
