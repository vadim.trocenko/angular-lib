export default function ngShow() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-show');

      const toggleVisibility = () => node.classList.toggle('ng-show', !scope.eval(variable));

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
