export default function ngRepeat() {
  return {
    link: (scope, node) => {
      const [name, data] = node.getAttribute('ng-repeat').split(' in ');
      const { parentNode: parent } = node;

      node.removeAttribute('ng-repeat');
      node.classList.add('repeating');
      node.remove();

      const nodeReplicator = () => {
        const fragment = document.createDocumentFragment();

        parent.querySelectorAll('.repeating').forEach(node => node.remove());

        for (const key of scope.eval(data)) {
          const item = node.cloneNode(true);

          scope[name] = key;
          scope.$$parser(item);
          item.innerHTML = item.innerHTML.replace(/{{(.*?)}}/g, (match, p1) => scope.$$parse(scope, p1));
          fragment.append(item);
          item.querySelectorAll('*').forEach(scope.$$compile);
        }
        parent.append(fragment);
      };

      nodeReplicator();
      scope.$watch(name, nodeReplicator);
    }
  };
}
