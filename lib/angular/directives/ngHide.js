export default function ngHide() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-hide');

      const toggleVisibility = () => node.classList.toggle('ng-hide', scope.eval(variable));

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
