export default function ngModel() {
  return {
    link: (scope, node, attrs) => {
      const variable = node.getAttribute('ng-model');
      const key = attrs.type === 'checkbox' ? 'checked' : 'value';
      const setVariable = () => {
        if (key === 'value') {
          scope.eval(`${variable} = '${node[key]}'`);
          return;
        }
        scope.eval(`${variable} = ${node[key]}`);
      };

      if (!scope[variable]) {
        setVariable();
      }

      const setInputValue = () => {
        node[key] = scope.eval(variable);
      };

      scope.$watch(variable, setInputValue);

      node.addEventListener('input', () => {
        setVariable();
        scope.$apply();
      });
    }
  };
}
