export default function ngBind() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-bind');
      const nodeText = () => {
        node.textContent = scope[variable];
      };

      nodeText();
      scope.$watch(variable, nodeText);
    }
  };
}
