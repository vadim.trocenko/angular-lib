function ngSelect() {
  return {
    link: (scope, node) => {
      const attributeValue = node.getAttribute('ng-select');
      const nodeValue = node.textContent;

      const textSelection = () => {
        node.textContent = nodeValue;
        const range = new Range();
        const index = node.firstChild.textContent.indexOf(scope.eval(attributeValue));
        const select = document.createElement('span');

        if (index < 0) {
          return;
        }

        range.setStart(node.firstChild, index);
        range.setEnd(node.firstChild, index + scope.eval(attributeValue).length);
        select.classList.add('ng-select');
        range.surroundContents(select);
      };

      textSelection();
      scope.$watch(attributeValue, textSelection);
    }
  };
}

export default ngSelect;
