export default function ngSrc() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-src');

      const updateUrl = () => {
        const evalValue = scope.eval(variable);

        if (!evalValue) {
          return;
        }

        node.src = evalValue;
      };

      updateUrl();
      scope.$watch(variable, updateUrl);
    }
  };
}
