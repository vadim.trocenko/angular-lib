export default function ngIf() {
  return {
    link: (scope, node) => {
      const variable = node.getAttribute('ng-if');

      const toggleVisibility = () => {
        node.hidden = !scope.eval(variable);
      };

      toggleVisibility();
      scope.$watch(variable, toggleVisibility);
    }
  };
}
