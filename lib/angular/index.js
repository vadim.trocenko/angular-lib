import * as defaultDirectives from './directives/index.js';
import * as utils from './utils/index.js';
import * as defaultFilters from './filters/index.js';
import * as defaultServices from './services/index.js';
import './style.css';

function SmallAngular() {
  const rootScope = window;
  const directives = {};
  const watchers = {};
  const controllers = {};
  const components = {};
  const services = {};
  const filters = {};
  const configs = [];

  function getDependencies(names) {
    return names.map(name => {
      if (name === '$rootScope' || name === '$scope') {
        return rootScope;
      }

      if (!services[name]) {
        throw new Error(`${name} service isn't registered!`);
      }

      return services[name];
    });
  }

  function directive(name, fn) {
    directives[utils.toCamelCaseParser(name)] = fn;

    return this;
  }

  function controller(name, fn) {
    controllers[name] = utils.setDependencies(fn);

    return this;
  }

  function component(name, fn) {
    components[utils.toCamelCaseParser(name)] = fn;

    return this;
  }

  function service(name, fn) {
    const funcWithDeps = utils.setDependencies(fn);
    const funcArgs = getDependencies(funcWithDeps.dependencies);

    services[name] = funcWithDeps(...funcArgs);

    return this;
  }

  const getAllAttributes = ({ attributes }) => {
    const allAttributes = {
      angularAttributes: {},
      notAngularAttributes: {}
    };

    for (const { name, value } of attributes) {
      const directiveName = utils.toCamelCaseParser(name);
      const attribute = directives[directiveName] ? 'angularAttributes' : 'notAngularAttributes';

      allAttributes[attribute][directiveName] = value;
    }

    return allAttributes;
  };

  function parse(scope, str) {
    const [variable, ...filterArr] = str.split('|').map(str => str.trim());
    const result = scope.eval(variable);

    return filterArr.reduce((prev, curr) => defaultFilters[curr](prev), result);
  }

  function constant(name, value) {
    services[name] = value;

    return this;
  }

  function config(fn) {
    const funcWithDeps = utils.setDependencies(fn);
    configs.push(funcWithDeps);

    return this;
  }

  const compile = node => {
    const { angularAttributes, notAngularAttributes } = getAllAttributes(node);
    const tag = utils.toCamelCaseParser(node.tagName.toLowerCase());
    const component = components[tag];


    if (component) {
      const { template, link, controller, controllerAs } = component();

      if (template) {
        node.innerHTML = template;
      }

      const Control = controllers[controller];

      if (!Control && link) {
        link(rootScope, node, notAngularAttributes);
      }

      if (!Control) {
        throw new Error(`${controller} isn't registered!`);
      }

      const controlDeps = getDependencies(Control.dependencies);
      const controlInst = new Control(...controlDeps);

      if (controllerAs) {
        rootScope[controllerAs] = controlInst;
      }

      node.querySelectorAll('*').forEach(compile);
    }

    for (const key in angularAttributes) {
      directives[key]().link(rootScope, node, notAngularAttributes);
    }
  };

  rootScope.$apply = () => {
    for (const key in watchers) {
      for (const callbackFn of watchers[key]) {
        callbackFn();
      }
    }
  };

  rootScope.$applyAsync = () => {
    setTimeout(rootScope.$apply, 50);
  };

  this.module = appName => {
    this.appName = appName;
    return this;
  };

  rootScope.$watch = (name, callback) => {
    if (!watchers[name]) {
      watchers[name] = [];
    }

    watchers[name].push(callback);
  };

  const initDefaultServices = () => {
    for (const key in defaultServices) {
      service(key, defaultServices[key]);
    }
  };

  const initDefaultConfigs = () => {
    configs.forEach(fn => {
      const funcArgs = getDependencies(fn.dependencies);
      fn(...funcArgs);
    });
  };

  this.bootstrap = (node = document.querySelector('[ng-app]')) => {
    node.querySelectorAll('*').forEach(item => compile(item));
  };

  rootScope.$$parse = parse;
  rootScope.$$parser = utils.componentParser;
  rootScope.$$compile = compile;
  this.directive = directive;
  this.constant = constant;
  this.config = config;
  this.controller = controller;
  this.component = component;
  this.service = service;


  initDefaultServices();
  Object.assign(directives, defaultDirectives);
  Object.assign(filters, defaultFilters);
  setTimeout(() => {
    initDefaultConfigs();
    this.bootstrap();
  }, 200);
}

const angular = new SmallAngular();
window.angular = angular;
export default angular;
