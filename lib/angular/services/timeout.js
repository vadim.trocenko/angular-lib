export default function $timeout($scope) {
  return function(fn, timeout) {
    setTimeout(() => {
      fn();
      $scope.$asyncApply();
    }, timeout);
  };
}

$timeout.dependencies = ['$scope'];
