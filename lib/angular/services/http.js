export default function $http($rootScope) {
  const METHODS = {
    GET: 'GET',
    POST: 'POST',
    DELETE: 'DELETE'
  };
  const resultTypeHandlers = {
    'blob': res => res.blob(),
    'json': res => res.json(),
    'text': res => res.text(),
    'arraybuffer': res => res.arrayBuffer()
  };

  class HttpRequest {
    constructor(url) {
      this.url = url;
    }

    get(path, config) {
      return this.request({ ...config, path, method: METHODS.GET });
    }

    post(path, data, config) {
      return this.request({ ...config, path, method: METHODS.POST, data });
    }

    delete(path, config) {
      return this.request({ ...config, path, method: METHODS.DELETE });
    }

    request(config) {
      const { path, method, responseType = 'text', data, headers = {} } = config;
      const url = new URL(path, this.url);

      return fetch(url.href, { method, body: data, headers })
        .then(res => {
          $rootScope.$applyAsync();

          if (!res.ok) {
            throw new Error(`${method} ${res.url} ${res.statusText}`);
          }

          return resultTypeHandlers[responseType](res);
        });
    }
  }

  return new HttpRequest();
}

$http.dependencies = ['$rootScope'];
